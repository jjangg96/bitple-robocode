package com.IU;

import java.awt.Color;

import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class IU extends Robot {
	/**
	 * run: IU's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setColors(Color.MAGENTA,Color.MAGENTA,Color.MAGENTA); // body,gun,radar

		// Robot main loop
		while (true) {
			// Replace the next 4 lines with any behavior you would like
			turnGunRight(180);
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		if (e.getDistance() < 50) {
			fire(3);
		} // otherwise, fire 1.
		else {
			fire(1);
		}
		ahead(getEnergy());
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		turnLeft(90);
		ahead(200);
	}

	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(getEnergy());
		turnLeft(getEnergy());
		ahead(30);
	}
}