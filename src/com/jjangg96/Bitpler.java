package com.jjangg96;

import java.awt.Color;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class Bitpler extends Robot {
	/**
	 * run: Kidulryeo's default behavior
	 */

	private int count = 0;
	private boolean isFoundEnemy = false;

	public void run() {

		setColors(Color.yellow, Color.yellow, Color.yellow);
		while (true) {
			// Replace the next 4 lines with any behavior you would like
			ahead(30);
			// turnGunRight(360);

			if (isFoundEnemy) {
				if (count % 2 == 0)
					turnLeft(45);
				else
					turnRight(45);

			} else {
				turnLeft(90);
			}

			isFoundEnemy = false;
			count++;
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {

		System.out.println("found");
		
		
		stop();

		if (e.getDistance() < 200)
			fire(3);
		else if (e.getDistance() < 500)
			fire(2);
		else
			fire(1);

		isFoundEnemy = true;
	}

	@Override
	public void onHitRobot(HitRobotEvent event) {
		fire(1);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		back(600);
		/*
		back(300);
		turnLeft(45);
		back(300);
		turnLeft(20);
		*/
	}

	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		System.out.println(e.getBearing());
		back(100);
		turnLeft(45);
	}
}
