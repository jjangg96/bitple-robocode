package com.kidulryeo;

import java.awt.Color;

import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

public class LOLO extends Robot {
	/**
	 * run: Kidulryeo's default behavior
	 */
	
	boolean peek;
	double moveAmount;
	public void run() {
		
		setBodyColor(Color.yellow);
		setGunColor(new Color(255, 051, 255));
		setRadarColor(new Color(0, 100, 100));
		setBulletColor(new Color(204, 0, 0));
		setScanColor(new Color(000, 204, 255));
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar

		// Robot main loop
		
		moveAmount = Math.max(getBattleFieldWidth(),getBattleFieldHeight());
		peek = false;
		
		turnLeft(getHeading()%90);
		ahead(moveAmount);
		
		peek=true;
		turnGunRight(90);
		turnRight(90);
		while (true) {
			// Replace the next 4 lines with any behavior you would like
			/*ahead(100);
			turnGunRight(360);
			back(100);
			turnGunRight(360);
			fire(20);*/
			peek = true;
			
			ahead(moveAmount);
			
			peek=false;
			
			turnRight(90);			
			turnGunRight(180);
			

		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(2);
		back(10);
		turnGunRight(180);
		if(peek){
			scan();
		}
		
	
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	/*public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
	}*/

public void onHitRobot(HitRobotEvent e){
	if(e.getBearing() > -90 && e.getBearing()<90){
		back(100);
	}
	else{
		ahead(100);
	}
}


	/**
	 * onHitWall: What to do when you hit a wall
	 */
	/*public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}*/
}